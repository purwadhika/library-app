import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookDetailsEditComponent } from './book-details-edit.component';

describe('BookDetailsEditComponent', () => {
  let component: BookDetailsEditComponent;
  let fixture: ComponentFixture<BookDetailsEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookDetailsEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookDetailsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
