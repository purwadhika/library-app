import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DataService } from '../data.service';

@Component({
  selector: 'app-book-details-edit',
  templateUrl: './book-details-edit.component.html',
  styleUrls: ['./book-details-edit.component.css']
})
export class BookDetailsEditComponent implements OnInit {

  ISBN: string = '';
  BookCategory: string;
  BookName:string;
  AuthorName: string;
  RackLocation: string;
  Price: number;
  Qty: number;
  Description: string;

  constructor(private activatedRoute: ActivatedRoute, private data:DataService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
        let isbn = params['isbn'];
        var obj = this.data.getBook(isbn);
        this.ISBN = obj['ISBN'];
        //
        //
        
      });
  }

}
