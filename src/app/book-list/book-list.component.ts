import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit {

  bookList : object[];

  constructor(private data:DataService, private router: Router) { }

  ngOnInit() {
    this.bookList = this.data.getBookList();
  }

  removeBook(isbn){
    this.bookList = this.data.removeBookList(isbn);
  }

  editBook(isbn){
    this.router.navigate(['/book-details-edit/', {isbn : isbn}]);
  }
}
