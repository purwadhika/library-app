import { Injectable } from '@angular/core';

@Injectable()
export class DataService {

  private bookList : Object[] = [{
    "ISBN":"1000",
    "BookCategory":"Fantasy",
    "BookName":"Hello World",
    "AuthorName":"",
    "RackLocation":"1",
    "Price":10000,
    "Qty":3,
    "Description":"-",
  }]

  constructor() { }

  getBookList():object[]
  {
    return this.bookList;
  }

  getBook(isbn:string):object
  {
    var obj:object;
    this.bookList.forEach(book => {
      if (book['ISBN'] == isbn) {
        obj = book;
      }
    });
    return obj;
  }

  addBookList(obj:object):object[]
  {
    this.bookList.push(obj);
    return this.bookList;
  }

  removeBookList(isbn:string):object[]
  {
    for (var i = 0; i < this.bookList.length; i++) {
      if (this.bookList[i]["ISBN"] == isbn) {
        this.bookList.splice(i, 1);
        break;
      } 
    }
    return this.bookList;
  }

}
