import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-book-details',
  templateUrl: './book-details.component.html',
  styleUrls: ['./book-details.component.css']
})
export class BookDetailsComponent implements OnInit {

  ISBN: string = '';
  BookCategory: string;
  BookName:string;
  AuthorName: string;
  RackLocation: string;
  Price: number;
  Qty: number;
  Description: string;

  constructor(private data:DataService) { }

  ngOnInit() {
  }

  onSubmit(myForm: NgForm) {
    console.log(myForm.value);  // { first: '', last: '' }
    console.log(myForm.valid);  // false
  }

  addBook(){
    this.data.addBookList({
      "ISBN":this.ISBN,
      "BookCategory":this.BookCategory,
      "BookName":this.BookName,
      "AuthorName":this.AuthorName,
      "RackLocation":this.RackLocation,
      "Price":this.Price,
      "Qty":this.Qty,
      "Description":this.Description,
    });
  }

}
