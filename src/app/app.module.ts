import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { DataService } from './data.service'

import { AppComponent } from './app.component';
import { BookListComponent } from './book-list/book-list.component';
import { BookDetailsComponent } from './book-details/book-details.component';
import { BookDetailsEditComponent } from './book-details-edit/book-details-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    BookListComponent, BookDetailsComponent, BookDetailsEditComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot([
        { path: '', component: BookListComponent },
        { path: 'book-details', component: BookDetailsComponent },
        { path: 'book-details-edit', component: BookDetailsEditComponent }
    ])
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
